#!/bin/bash

# Function to check and install Node.js via nvm
check_and_install_node() {
    if ! command -v node &> /dev/null || ! command -v npm &> /dev/null; then
        echo "Node.js is not installed. Would you like to install Node.js now? (yes/no)"
        read -r install_node
        if [ "$install_node" == "yes" ]; then
            echo "Installing nvm and Node.js..."
            curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
            export NVM_DIR="$HOME/.nvm"
            [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
            nvm install node
            nvm use node
        else
            echo "Node.js is required to run KADI. Exiting..."
            exit 1
        fi
    else
        echo "Node.js is already installed."
    fi
}

# Function to check and install Git
check_and_install_git() {
    if ! command -v git &> /dev/null; then
        echo "Git is not installed. Would you like to install Git now? (yes/no)"
        read -r install_git
        if [ "$install_git" == "yes" ]; then
            echo "Installing Git..."
            if [ "$(uname)" == "Darwin" ]; then
                brew install git
            else
                sudo apt-get install git -y
            fi
        else
            echo "Git is required to clone repositories. Exiting..."
            exit 1
        fi
    else
        echo "Git is already installed."
    fi
}

# Check and install Node.js and Git if necessary
check_and_install_node
check_and_install_git

# Store the current working directory
INSTALL_DIR=$(pwd)
echo "Installation directory: $INSTALL_DIR"

# Ask user if they want to install and use a local kadi-server
echo "Do you want to install and use a local kadi-server? (yes/no)"
read -r use_local_server

if [ "$use_local_server" == "yes" ]; then
    # Clone kadi-server repository
    git clone https://gitlab.com/humin-game-lab/kadi/kadi-server.git "$INSTALL_DIR/kadi-server"
    cd "$INSTALL_DIR/kadi-server" || { echo "Failed to navigate to kadi-server directory."; exit 1; }
    npm install

    # Start server in background and save PID
    nohup node server.js > server.log 2>&1 & SERVER_PID=$!
    echo "kadi-server started with PID $SERVER_PID"

    # Pause to give server time to start
    sleep 5
else
    # Ask for the public server URL and port
    echo "Please provide the URL and port for the public server (in the form http://myserver.com:port):"
    read -r SERVER_URL
fi

# Change back to install directory
cd "$INSTALL_DIR" || { echo "Failed to navigate to install directory."; exit 1; }

# Clone kadi repository
git clone https://gitlab.com/humin-game-lab/kadi/kadi.git "$INSTALL_DIR/kadi"
cd "$INSTALL_DIR/kadi" || { echo "Failed to navigate to kadi directory."; exit 1; }

# If using a remote server, update the agent.json file
if [ "$use_local_server" == "no" ]; then
    if [ -f "agent.json" ]; then
        echo "Updating agent.json to use the remote server URL."
        sed -i.bak "s|\"api\": \".*\"|\"api\": \"$SERVER_URL\"|" agent.json
    else
        echo "Warning: agent.json file not found. Skipping update."
    fi
fi

# Install dependencies and setup KADI CLI
npm install
cd abilities/kadi-install/0.0.0 || { echo "Failed to navigate to abilities/kadi-install/0.0.0 directory."; exit 1; }
npm install

# Link the kadi CLI
cd "$INSTALL_DIR/kadi" || { echo "Failed to navigate to kadi root directory."; exit 1; }
npm link

# Run initial KADI setup commands
kadi install
kadi update

# If using a local server, restart the server in foreground
if [ "$use_local_server" == "yes" ]; then
    kill $SERVER_PID
    wait $SERVER_PID 2>/dev/null
    echo "Restarting kadi-server in foreground..."
    node server.js
fi

echo "KADI CLI and server setup is complete."

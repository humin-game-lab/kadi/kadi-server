@echo off

REM Function to check and install Node.js
:check_node
where node >nul 2>&1
IF %ERRORLEVEL% NEQ 0 (
    echo Node.js is not installed. Would you like to install Node.js now? (yes/no)
    set /p install_node=
    IF /I "%install_node%"=="yes" (
        echo Installing Node.js...
        REM Install Node.js via Chocolatey (requires admin privileges)
        powershell -Command "Start-Process powershell -ArgumentList 'choco install nodejs-lts -y' -Verb runAs"
    ) ELSE (
        echo Node.js is required to run KADI. Exiting...
        exit /b 1
    )
) ELSE (
    echo Node.js is already installed.
)
goto :check_git

REM Function to check and install Git
:check_git
where git >nul 2>&1
IF %ERRORLEVEL% NEQ 0 (
    echo Git is not installed. Would you like to install Git now? (yes/no)
    set /p install_git=
    IF /I "%install_git%"=="yes" (
        echo Installing Git...
        REM Install Git via Chocolatey (requires admin privileges)
        powershell -Command "Start-Process powershell -ArgumentList 'choco install git -y' -Verb runAs"
    ) ELSE (
        echo Git is required to clone repositories. Exiting...
        exit /b 1
    )
) ELSE (
    echo Git is already installed.
)
goto :set_install_dir

REM Store the current working directory
:set_install_dir
set "INSTALL_DIR=%cd%"
echo Installation directory: %INSTALL_DIR%

REM Ask user if they want to install and use a local kadi-server
set /p use_local_server=Do you want to install and use a local kadi-server? (yes/no) 

IF /I "%use_local_server%"=="yes" (
    REM Clone kadi-server repository
    git clone https://gitlab.com/humin-game-lab/kadi/kadi-server.git "%INSTALL_DIR%\kadi-server"
    cd "%INSTALL_DIR%\kadi-server" || (
        echo Failed to navigate to kadi-server directory.
        exit /b 1
    )
    npm install

    REM Start server in background and save PID
    start /b node server.js > server.log 2>&1
    set "SERVER_PID=%!"

    REM Pause to give server time to start
    timeout /t 5 /nobreak >nul
) ELSE (
    REM Ask for the public server URL and port
    set /p SERVER_URL=Please provide the URL and port for the public server (in the form http://myserver.com:port): 
)
goto :clone_kadi

REM Clone kadi repository
:clone_kadi
cd "%INSTALL_DIR%" || (
    echo Failed to navigate to install directory.
    exit /b 1
)
git clone https://gitlab.com/humin-game-lab/kadi/kadi.git "%INSTALL_DIR%\kadi"
cd "%INSTALL_DIR%\kadi" || (
    echo Failed to navigate to kadi directory.
    exit /b 1
)

REM If using a remote server, update the agent.json file
IF /I "%use_local_server%"=="no" (
    if exist "agent.json" (
        echo Updating agent.json to use the remote server URL.
        powershell -Command "(Get-Content agent.json) -replace '\"api\": \".*\"', '\"api\": \"%SERVER_URL%\"' | Set-Content agent.json"
    ) ELSE (
        echo Warning: agent.json file not found. Skipping update.
    )
)

REM Install dependencies and setup KADI CLI
npm install
cd abilities\kadi-install\0.0.0 || (
    echo Failed to navigate to abilities\kadi-install\0.0.0 directory.
    exit /b 1
)
npm install

REM Link the kadi CLI
cd "%INSTALL_DIR%\kadi" || (
    echo Failed to navigate to kadi root directory.
    exit /b 1
)
npm link

REM Run initial KADI setup commands
kadi install
kadi update

REM If using a local server, restart the server in foreground
IF /I "%use_local_server%"=="yes" (
    taskkill /PID %SERVER_PID% /F
    echo Restarting kadi-server in foreground...
    node server.js
)

echo KADI CLI and server setup is complete.
pause

# kadi-server



Using the Server for Testing
---------------------------------

The server is designed to simulate a backend that serves agent abilities and their metadata, including dependencies, `init` commands, and `run` commands. It enables testing the `kadi` tool's functionality in a controlled environment.

### Setup

1.  Change directory to the server root folder and perform install (only if repo was manually downloaded, setup script already performs `npm install` on server)
    
    `npm install`

2.  Ensure the mock server (`server.js`) is running:
    
    `node server.js`
    
    The server will start on a predefined port (e.g., `3000`). Ensure `kadi` is configured to point to this server for testing.
    
3.  Update your `kadi` tool's configuration to use the mock server's URL for fetching abilities.  This can be updated in tke kadi/`agent.json`, `api` field to `"http://localhost:3000"`
    

Test Server Endpoints
=====================

The kadi test server provides several endpoints to assist in the development and testing of the kadi CLI tool. These endpoints allow developers to download abilities, retrieve installation scripts, and search for specific abilities.

`/agents`
---------

This endpoint serves zip downloads for the available abilities, along with a sample `agent.json`.

### Folder Structure

        ```lua
        abilities/ 
        |-- echo/ 
        |   |-- 0.0.1
        |   |-- 1.0.0 
        |-- install/     
        |   |-- 0.0.0
        |   |-- 1.0.0
        |-- kadi-install/     
        |   |-- 0.0.1
        ```

### Example Usage

To download the `echo_0.0.1.zip` ability:

`wget http://localhost:[PORT]/abilities/echo/0.0.1/echo_0.0.1.zip`

Replace `[PORT]` with the port number your test server is running on.

`/install`
----------

Provides access to installation scripts for both Linux and Windows.

### Folder Structure

```lua
install/ 
|-- linux/ 
|   |-- install_kadi_linux.sh 
|-- windows/     
    |-- install_kadi_win.ps1
```

### Example Usage

To download the Linux installation script:

bashCopy code

`wget http://localhost:[PORT]/install/linux/install_kadi_linux.sh`

Make sure to replace `[PORT]` with your test server's port number.

`/get`
---------

Accepts an ability name and version as a query parameter (`ability`) and returns the corresponding ability's JSON object. Currently supports 3 test abilities: `echo`, `install` and `kadi-install`.  If no version is provided, it will return the latest version of ability.

### Example Usage

To get for the `echo v1.0.0` ability:


`curl "http://localhost:[PORT]/get?ability=echo&version=1.0.0"`

This will return the JSON object associated with the `echo v1.0.0` ability:

```json
{
  "name": "echo",
  "version": "1.0.0",
  "license": "MIT",
  "description": "test echo ability for kadi",
  "repo": "https://example.com/repo/echo.git",
  "lib": "http://localhost:3000/abilities/echo/1.0.0/echo_1.0.0.zip",
  "abilities": [
    {
      "name": "echo",
      "version": "0.0.1"
    }
  ],
  "bin": {
    "echo": "abilities/echo/1.0.0"
  },
  "init": "echo 'echo v1.0.0 Installed...'; pwd",
  "start": "echo 'Starting echo v1.0.0...'; pwd",
  "run": {
    "greet": "echo 'Hello, World from echo v1.0.0!'"
  }
}
```

Replace `[PORT]` with the appropriate port number.

`/git`
---------

This will perform a `git pull` on the repo where the server is running.  The goal of this is to pull in any new agent.json or files that have been pushed to the git repot to now be pulled down and accessible for serving.  This is a development endpoint, and is to make it easy for a development team to push new abilities to the team via the kadi interface.  A developer would commit their agent.json to the server repo, then call the `/git` endpoint, which would cause the team server to pull those new files down and making it available to anyone that has access to the server.

### Example Usage

To update the server with the lates files from the git reop


`curl "http://localhost:[PORT]/git"`

This will return the JSON object associated the git pull command:

* On success:
```json
    {
      "success": true,
      "message": "Application code is updated.",
      "stdout": "gitStdout",
      "stderr": "gitStderr"
    }
```

* On Failure:
```json
    {
      "success": false,
      "message": "Failed to update the application code.",
      "error": "gitError.message",
      "stdout": "gitStdout",
      "stderr": "gitStderr"
    }
```

Roadmap Features
---------

* **KADI SERVER** 
    * ~~Breakout server code into its own repo~~
    * ~~add /update endpoint that will cause server to do a `git pull` and update the server~~
        * ~~Could add ability to restart server, but that is not needed to just get the updated agent.json's in the repo.~~
          * ~~removing the "restart" portion of this update for now.~~
        * ~~this will enable people to push new agent.json's to the repo and then cause server to restart to make them avaialbe to everyone~~

       
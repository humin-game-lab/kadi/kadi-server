import express from 'express';
import { get } from 'http';
import path from 'path';
import { fileURLToPath } from 'url';
import fs from 'fs';
import { exec } from 'child_process';
import cors from 'cors';


const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const kadiCliPath = process.env.KADI_CLI_PATH;
let kadiAgentPath = null;
if (!kadiCliPath) {
  console.error('The KADI_CLI_PATH environment variable is not set.');
//   process.exit(1);
}
else{
    kadiAgentPath  = path.join(kadiCliPath, 'agent.json');
}


const app = express();
app.use(cors());


function getAbilityJSON(abilityName, abilityVersion){
    console.log("getAbilityJSON: " + JSON.stringify(abilityName) + " v" + abilityVersion);
    //Get the agent.json for the ability
    let abilityPath = "";
    let version = abilityVersion;
   
    if(version == null){//Need to find the latetest version in directory
        
        //Get the ability directory, has list of versions
        abilityPath = path.join(__dirname, 'abilities', abilityName);

        let versions;
        //Read the directory and get the latest version
        try{
            versions = fs.readdirSync(abilityPath);
        }
        catch(e){
            versions = null;
            console.log("Agent.json not found at path: " + abilityPath);
            return versions;
        }

        //sort the versions and get the latest
        versions = versions.sort();
        
        //Get the latest version
        version = versions[versions.length-1];
        
    }
    
    //Create the path to the agent.json with latest version
    abilityPath = path.join(__dirname, 'abilities', abilityName, version, 'agent.json');

    let abilityJSON;
    try{
        abilityJSON = fs.readFileSync(abilityPath);
        
    }
    catch(e){
        abilityJSON = null;
        console.log("Agent.json not found at path: " + abilityPath);
        return abilityJSON;
   } 
    abilityJSON = JSON.parse(abilityJSON);

    return abilityJSON;

}

//Gets the agent.json for the ability, so dependencies can be installed
function getProjectJSON(){
    //Get the agent.json for kadi
    let projectJSON = fs.readFileSync(projectAgentPath, 'utf8');
    projectJSON = JSON.parse(projectJSON);
    return projectJSON;

}
// Serve static files from the agents directory
// app.use('/agents', express.static(path.join(__dirname, 'agents'))); //Depricated
app.use('/install', express.static(path.join(__dirname, 'install')));
app.use('/abilities', express.static(path.join(__dirname, 'abilities')));

app.get('/get', (req, res) => {
    console.log(req.query);
    const { ability, version } = req.query;
    
    //Get the agent.json for the ability
    let abilityJSON = getAbilityJSON(ability, version);
    if(abilityJSON){
        res.json(abilityJSON);
    }
    else{
        res.status(404).json({ error: "Ability not found" });
    }
});

app.get('/git', (req, res) => {
    // Pull the latest code from your Git repository
    exec('git pull', (gitError, gitStdout, gitStderr) => {
        if (gitError) {
            console.error(`git pull error: ${gitError}`);
            // Send the error in a JSON object
            return res.status(500).json({
                success: false,
                message: 'Failed to update the application code.',
                error: gitError.message,
                stdout: gitStdout,
                stderr: gitStderr
            });
        }
        console.log(`git pull stdout: ${gitStdout}`);
        console.error(`git pull stderr: ${gitStderr}`);
        
        // Send the git pull output in a nicely formatted JSON object
        res.json({
            success: true,
            message: 'Application code is updated.',
            stdout: gitStdout,
            stderr: gitStderr
        });
    });
});

//Gets the agent.json for the ability, so dependencies can be installed
function getKadiJSON(){
    let kadiJSON = null
    //Get the agent.json for kadi
    if(kadiAgentPath){
        kadiJSON = fs.readFileSync(kadiAgentPath, 'utf8');
        kadiJSON = JSON.parse(kadiJSON);
    }
    
    return kadiJSON;

}
// Get the API URL from the agent.json
let kadijson = getKadiJSON();

const PORT = kadijson ? kadijson.broker.port : 8000;
const HOST = kadijson ? kadijson.broker.host : 'localhost';

console.log(`Kadi Server running on http://${HOST}:${PORT}`);
app.listen(PORT, () => {
  console.log(`Server running on http://${HOST}:${PORT}`);
});

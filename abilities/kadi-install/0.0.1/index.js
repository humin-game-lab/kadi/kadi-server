// Base INSTALL ability module for kadi, used to perform initial installation of abilities
import { fileURLToPath } from 'url';
import path from 'path';
import fs from 'fs';
import { promises as fsPromises } from 'fs';
import chalk from 'chalk';
import { exec } from 'child_process';
import AdmZip from 'adm-zip';
import axios from 'axios';

// Define the API URL (Adjust according to your API)
const SEARCH_API_URL    = 'http://localhost:3000/search';
const GET_API_URL       = 'http://localhost:3000/get';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const rootDir           = path.join(__dirname, '../../../');
const abilitiesDir      = path.join(rootDir, './abilities/');
const kadiAgentPath      = path.join(rootDir, 'agent.json');


//Updates the agent.json of kadi with the installed ability version
function updateBin(moduleName, version) {
    let abilityJSON = {};
    let kadiAgentJSON = {};
    // Read the current agent.json's
    try {
        abilityJSON = getAbilityJSON(moduleName, version);
        kadiAgentJSON = getKadiJSON();

    } catch (error) {
        console.error(`Error reading agent.json: ${error}`);
        return;
    }

    // Add all Ability bin commands to the kadi bin
    if(abilityJSON.bin){
          for (const command in abilityJSON.bin) {
            console.log(`Adding ${command} to kadi bin with path ${abilityJSON.bin[command]}`);
            kadiAgentJSON.bin[command] = abilityJSON.bin[command];
          }
    }

    // Write the updated config back to config.json
    try {
        const data = JSON.stringify(kadiAgentJSON, null, 2); // Beautify the JSON output
        fs.writeFileSync(kadiAgentPath, data);
        console.log(`agent.json updated for ${moduleName} to version ${version}.`);
    } catch (error) {
        console.error(`Error writing to agent.json: ${error}`);
    }
}

//Gets the agent.json for the ability, so dependencies can be installed
function getAbilityJSON(abilityName, abilityVersion){
    //Get the agent.json for the ability
    let abilityPath = path.join(rootDir, 'abilities', abilityName, abilityVersion, 'agent.json');
    let abilityJSON = fs.readFileSync(abilityPath, 'utf8');
    abilityJSON = JSON.parse(abilityJSON);

    return abilityJSON;

}
//Gets the agent.json for the ability, so dependencies can be installed
function getKadiJSON(){
    //Get the agent.json for kadi
    let kadiJSON = fs.readFileSync(kadiAgentPath, 'utf8');
    kadiJSON = JSON.parse(kadiJSON);
    return kadiJSON;

}

//Runs a command line string, used for abilities init functions
async function runCommand(name, version, command) {
    const abilityDir = path.join(rootDir, 'abilities', name, version);
    return new Promise((resolve, reject) => {
        exec(command, { cwd: abilityDir },(error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                reject(error); // Reject the promise on error
                return;
            }
            if (stdout.trim()) {
                console.log(stdout);
            }
            if (stderr.trim()) {
                console.error(stderr);
            }
            resolve(); // Resolve the promise when exec completes successfully
        });
    });
}

// Ensure the abilities directory exists
async function ensureAbilitiesDirectory() {
    const dirPath = path.join(process.cwd(), 'abilities');
    try {
        await fsPromises.access(dirPath);
    } catch (error) {
        await fsPromises.mkdir(dirPath, { recursive: true });
        console.log(chalk.yellow('Created the abilities directory.'));
    }
}

//Installs the ability from the agent.json of kadi
async function installKadiAbilities(){

    //Get the abilities from the agent.json
    let kadiAgentJSON = getKadiJSON()
    let kadiAbilities = kadiAgentJSON.abilities;


    //Install the abilities
    for (const ability of kadiAbilities) {

        await installAbility(ability);

        //Update the agent.json bin with the installed ability
        updateBin(ability.name, ability.version);
        
    }
        
}

// Download and extract library
async function downloadAndExtractLibrary({ url, ability }) {
    console.log(chalk.green(`Downloading ${ability.name} v${ability.version}\n from: ${url} ...`));
    const response = await axios.get(url, { responseType: 'arraybuffer' });
    
    // console.log(chalk.green(`Extracting ${ability.name} v${ability.version}...`));
    const zip = new AdmZip(response.data);
    const extractPath = path.join(abilitiesDir, ability.name , ability.version);
    zip.extractAllTo(extractPath, true);
    
    console.log(chalk.green(`${ability.name} version ${ability.version} has been extracted to ${extractPath}`));
}

//Recursive function to install abilities and their dependencies
async function installAbility(ability){
    await ensureAbilitiesDirectory();
    const abilityDirPath = path.join(abilitiesDir, ability.name , ability.version);
    try {
        await fsPromises.access(abilityDirPath);
        console.log(chalk.yellow(`${ability.name} v${ability.version} is already installed.`));
        // Even if installed, you may still want to run the init command if dependencies might have changed
    } 
    catch (error) {
        // Directory does not exist, implying ability is not installed
        console.log(chalk.blue(`Installing ${ability.name} v${ability.version}...`));
        try {
            
            const response = await axios.get(`${GET_API_URL}?ability=${ability.name}&version=${ability.version}`);
            if (response.status === 200) {
                const { name, lib, version, init } = response.data;

                if (lib) await downloadAndExtractLibrary({ url: lib, ability: {name: name, version: version}});

                // Run init command for dependencies recursively first (assuming dependencies are included in response)
                if (response.data.abilities) {
                    for (const ability of response.data.abilities) {
                        await installAbility(ability);
                    }
                }
  
                // After all dependencies are installed, run the init command if it exists
                if (init) {
                    console.log(chalk.green(`Running init for ${ability.name} v${ability.version}...`));
                    await runCommand(name, version, init);
                }
            } else {
                console.log(chalk.red('Ability not found.'));
            }
        } 
        catch (error) {
            console.error(chalk.red('An error occurred:'), error.message);
        }
    }   
    
}

//Entry point for module, installs abilities from agent.json
export default async function(args) {
    const [moduleName, version, ...flags] = args;

    //Perform install based upon abilities in agent.json from root directory
    if(moduleName === undefined) {
        console.log("Installing abilities from kadi agent.json");
        installKadiAbilities();
    }
    else{
        console.log(`Can't install ${moduleName} version ${version} with flags ${flags}\nMust install KADI Abilities using 'kadi install' before installing any modules.`);

    }
};

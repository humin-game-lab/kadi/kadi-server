// Base INSTALL ability module for apm, used to perform initial installation of abilities
import { fileURLToPath } from 'url';
import path from 'path';
import fs from 'fs';
import { promises as fsPromises } from 'fs';
import chalk from 'chalk';
import { exec } from 'child_process';
import AdmZip from 'adm-zip';
import axios from 'axios';

// Define the API URL (Adjust according to your API)
// const API_URL = 'https://agentability.com/search';
const SEARCH_API_URL    = 'http://localhost:3000/search';
const GET_API_URL       = 'http://localhost:3000/get';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const rootDir           = path.join(__dirname, '../../../');
const abilitiesDir      = path.join(rootDir, './abilities/');
const installDir        = path.join(__dirname, './');
const configPath        = path.join(rootDir, 'config.json');
const apmAgentPath      = path.join(rootDir, 'agent.json');
const installAgentPath = path.join(installDir, 'agent.json');

console.log("abilitiesDir: ", abilitiesDir);

let installAgentJSON = fs.readFileSync(installAgentPath);
installAgentJSON = JSON.parse(installAgentJSON);

//Updates the config.json of apm with the installed ability version
function updateConfig(moduleName, version) {

    // Read the current config
    let config = {};
    try {
        const rawData = fs.readFileSync(configPath);
        config = JSON.parse(rawData);
    } catch (error) {
        console.error(`Error reading config.json: ${error}`);
        return;
    }

    // Update the module version
    config[moduleName] = version;

    // Write the updated config back to config.json
    try {
        const data = JSON.stringify(config, null, 2); // Beautify the JSON output
        fs.writeFileSync(configPath, data);
        console.log(`Config updated for ${moduleName} to version ${version}.`);
    } catch (error) {
        console.error(`Error writing to config.json: ${error}`);
    }
}

//Gets the agent.json for the ability, so dependencies can be installed
function getAbilityJSON(abilityName, abilityVersion){
    //Get the agent.json for the ability
    let abilityPath = path.join(rootDir, 'abilities', abilityName, abilityVersion, 'agent.json');
    let abilityJSON = fs.readFileSync(abilityPath);
    abilityJSON = JSON.parse(abilityJSON);

    return abilityJSON;

}

//Runs a command line string, used for abilities init functions
async function runCommand(command) {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                reject(error); // Reject the promise on error
                return;
            }
            if (stdout.trim()) {
                console.log(stdout);
            }
            if (stderr.trim()) {
                console.error(stderr);
            }
            resolve(); // Resolve the promise when exec completes successfully
        });
    });
}

// Ensure the abilities directory exists
async function ensureAbilitiesDirectory() {
    const dirPath = path.join(process.cwd(), 'abilities');
    try {
        await fsPromises.access(dirPath);
    } catch (error) {
        await fsPromises.mkdir(dirPath, { recursive: true });
        console.log(chalk.yellow('Created the abilities directory.'));
    }
}

//Installs the ability from the agent.json of apm
async function installAPMAbilities(){

    //Get the abilities from the agent.json
    let apmAgentJSON = fs.readFileSync(apmAgentPath);
    apmAgentJSON = JSON.parse(apmAgentJSON);
    let apmAbilities = apmAgentJSON.abilities;

    //Install the abilities
    for (const ability of apmAbilities) {
        console.log(chalk.blue(`Installing ${ability.name} version ${ability.version}`));

        installAbility(ability);

        //Call the init function for the ability
            //Get the agent.json for the ability
        let abilityJSON = getAbilityJSON(ability.name, ability.version);

            //Get the init command from the agent.json
        let initCommand = abilityJSON.init;

            //Run the init command
        if (initCommand) {
        
            try {
                console.log(chalk.green(`Running init command for ${ability.name} version ${ability.version}`));
                await runCommand(initCommand); // Wait for the command to complete
            } 
            catch (error) {
                console.error(chalk.red(`An error occurred while running the init command for ${ability.name}: ${error}`));
            }
        }

        //Update the config.json with the installed ability
        updateConfig(ability.name, ability.version);
        
    }
        
}

// Download and extract library
async function downloadAndExtractLibrary({ url, ability }) {
    console.log(chalk.green(`Downloading ${ability.name} v${ability.version} ...`));
    const response = await axios.get(url, { responseType: 'arraybuffer' });
    
    console.log(chalk.green(`Extracting ${ability.name} v${ability.version}...`));
    const zip = new AdmZip(response.data);
    const extractPath = path.join(abilitiesDir, ability.name , ability.version);
    zip.extractAllTo(extractPath, true);
    
    console.log(chalk.green(`${ability.name} version ${ability.version} has been extracted to ${extractPath}`));
}

//Recursive function to install abilities and their dependencies
    //May need to convert this to a promise, like runCommand, to make sure it installs in order
async function installAbility(ability){
    await ensureAbilitiesDirectory();
    const abilityDirPath = path.join(abilitiesDir, ability.name , ability.version);
    try {
        await fsPromises.access(abilityDirPath);
        console.log(chalk.yellow(`${ability.name} v${ability.version} is already installed.`));
        // Even if installed, you may still want to run the init command if dependencies might have changed
    } 
    catch (error) {
        // Directory does not exist, implying ability is not installed
        console.log(chalk.blue(`Installing ${ability.name} v${ability.version}...`));
        try {
            const response = await axios.get(`${GET_API_URL}?ability=${ability}&version=${ability.version}`);
            if (response.status === 200) {
                const { name, lib, version, init } = response.data;
                if (lib) await downloadAndExtractLibrary({ url: lib, name });

                // Not needed, as we are only installing from apm agent.json, we are not adding
                //  new abilities via CLI... the full install needs this ability
                // if(type === 'specific')
                //     await updateAgentJson(name, version);
                
                // Run init command for dependencies recursively first (assuming dependencies are included in response)
                if (response.data.abilities) {
                    for (const ability of response.data.abilities) {
                        await installAbility(ability);
                    }
                }
  
                // After all dependencies are installed, run the init command if it exists
                if (init) {
                    console.log(chalk.green(`Running init for ${ability.name} v${ability.version}...`));
                    await runCommand(init);
                }
            } else {
                console.log(chalk.red('Ability not found.'));
            }
        } 
        catch (error) {
            console.error(chalk.red('An error occurred:'), error.message);
        }
    }   
    
}

//Entry point for module, installs abilities from agent.json
export default async function(args) {
    const [moduleName, version, ...flags] = args;

    //Perform install based upon abilities in agent.json from root directory
    if(moduleName === undefined) {
        console.log("Installing abilities from apm agent.json");
        installAPMAbilities();
    }
    else{
        console.log(`Can't install ${moduleName} version ${version} with flags ${flags}\nMust install APM Abilities using 'agent install' before installing any modules.`);

    }
};
